//
// Created by aljosa on 6. 02. 20.
//

#include "main.h"
#include "ethernetParser.h"


unsigned char empty[] = {};
TestCase emptyCase = {
        .isipv4 = false,
        .optionsPresent = false,
        .protocol = 0,
        .dscp = 0,
        .packetSize = sizeof(empty) / sizeof(empty[0]),
        .packet = empty
};

unsigned char justEthernetHeader[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                        0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                        0x08, 0x00};                                                                                        // type
TestCase justEthernetHeaderCase = {
        .isipv4 = false,
        .optionsPresent = false,
        .protocol = 0,
        .dscp = 0,
        .packetSize = sizeof(justEthernetHeader) / sizeof(justEthernetHeader[0]),
        .packet = justEthernetHeader
};

unsigned char notEnoughData[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                                 0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                                 0x08, 0x00,                                                                                         // type
                                 (4 << 4) | 5,                                                                                       // version | length
                                 (3 << 2) | 2,                                                                                       // dscp & ecn
                                 0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0xFF,
                                 0x70,                                                                                               // protocol
                                 0x19, 0xE1, 0xC0, 0xA8, 0x00, 0x0A, 0xE0, 0x00,
                                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
TestCase notEnoughDataResult = {
        .isipv4 = false,
        .optionsPresent = false,
        .protocol = 0,
        .dscp = 0,
        .packetSize = sizeof(notEnoughData) / sizeof(notEnoughData[0]),
        .packet = notEnoughData
};

unsigned char legacyEthernet[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                        0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                        0x05, 0xDC,                                                                                         // length - 1500
                        (6 << 4) | 5,                                                                                       // version | length
                        (0x30 << 2) | 0,                                                                                    // dscp & ecn
                        0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0xFF,
                        0x70,                                                                                               // protocol
                        0x19, 0xE1, 0xC0, 0xA8, 0x00, 0x0A, 0xE0, 0x00,
                        0x00, 0x12, 0x21, 0x01, 0xC8, 0x01, 0x00, 0x01, 0x56, 0x52, 0xC0, 0xA8, 0x00, 0x01, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
TestCase legacyEthernetCase = {
        .isipv4 = false,
        .optionsPresent = false,
        .protocol = 0,
        .dscp = 0,
        .packetSize = sizeof(legacyEthernet) / sizeof(legacyEthernet[0]),
        .packet = legacyEthernet
};


unsigned char invalidType[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                                  0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                                  0x05, 0xEB,                                                                                         // invalid type - 1515
                                  (6 << 4) | 5,                                                                                       // version | length
                                  (0x30 << 2) | 0,                                                                                    // dscp & ecn
                                  0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0xFF,
                                  0x70,                                                                                               // protocol
                                  0x19, 0xE1, 0xC0, 0xA8, 0x00, 0x0A, 0xE0, 0x00,
                                  0x00, 0x12, 0x21, 0x01, 0xC8, 0x01, 0x00, 0x01, 0x56, 0x52, 0xC0, 0xA8, 0x00, 0x01, 0x00, 0x00,
                                  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
TestCase invalidTypeCase = {
        .isipv4 = false,
        .optionsPresent = false,
        .protocol = 0,
        .dscp = 0,
        .packetSize = sizeof(invalidType) / sizeof(invalidType[0]),
        .packet = invalidType
};


unsigned char ipv6[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                        0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                        0x08, 0x00,                                                                                         // type
                        (6 << 4) | 5,                                                                                       // version | length
                        (0x30 << 2) | 0,                                                                                    // dscp & ecn
                        0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0xFF,
                        0x70,                                                                                               // protocol
                        0x19, 0xE1, 0xC0, 0xA8, 0x00, 0x0A, 0xE0, 0x00,
                        0x00, 0x12, 0x21, 0x01, 0xC8, 0x01, 0x00, 0x01, 0x56, 0x52, 0xC0, 0xA8, 0x00, 0x01, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
TestCase ipv6Case = {
        .isipv4 = false,
        .optionsPresent = false,
        .protocol = 0,
        .dscp = 0,
        .packetSize = sizeof(ipv6) / sizeof(ipv6[0]),
        .packet = ipv6
};

unsigned char notIpv4Type[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                         0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                         0x08, 0x01,                                                                                         // type
                         (4 << 4) | 5,                                                                                       // version | length
                         (0x30 << 2) | 0,                                                                                    // dscp & ecn
                         0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0xFF,
                         0x70,                                                                                               // protocol
                         0x19, 0xE1, 0xC0, 0xA8, 0x00, 0x0A, 0xE0, 0x00,
                         0x00, 0x12, 0x21, 0x01, 0xC8, 0x01, 0x00, 0x01, 0x56, 0x52, 0xC0, 0xA8, 0x00, 0x01, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
TestCase notIpv4TypeCase = {
        .isipv4 = false,
        .optionsPresent = false,
        .protocol = 0,
        .dscp = 0,
        .packetSize = sizeof(notIpv4Type) / sizeof(notIpv4Type[0]),
        .packet = notIpv4Type
};

unsigned char valid[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                        0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                        0x08, 0x00,                                                                                         // type
                        (4 << 4) | 5,                                                                                       // version | length
                        (0x30 << 2) | 0,                                                                                    // dscp & ecn
                        0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0xFF,
                        0x70,                                                                                               // protocol
                        0x19, 0xE1, 0xC0, 0xA8, 0x00, 0x0A, 0xE0, 0x00,
                        0x00, 0x12, 0x21, 0x01, 0xC8, 0x01, 0x00, 0x01, 0x56, 0x52, 0xC0, 0xA8, 0x00, 0x01, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
TestCase validCase = {
        .isipv4 = true,
        .optionsPresent = false,
        .protocol = 0x70,
        .dscp = 0x30,
        .packetSize = sizeof(valid) / sizeof(valid[0]),
        .packet = valid
};

unsigned char vlanTag[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                         0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                         0x81, 0x00, 0x00, 0x00,                                                                             // vlan tag
                         0x08, 0x00,                                                                                         // type
                         (4 << 4) | 5,                                                                                       // version | length
                         (0x30 << 2) | 0,                                                                                    // dscp & ecn
                         0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0xFF,
                         0x70,                                                                                               // protocol
                         0x19, 0xE1, 0xC0, 0xA8, 0x00, 0x0A, 0xE0, 0x00,
                         0x00, 0x12, 0x21, 0x01, 0xC8, 0x01, 0x00, 0x01, 0x56, 0x52, 0xC0, 0xA8, 0x00, 0x01, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

TestCase vlanTagCase = {
        .isipv4 = true,
        .optionsPresent = false,
        .protocol = 0x70,
        .dscp = 0x30,
        .packetSize = sizeof(vlanTag) / sizeof(vlanTag[0]),
        .packet = vlanTag
};

unsigned char vlan2Tag[] = {0x01, 0x00, 0x5E, 0x00, 0x00, 0x12,
                           0x00, 0x00, 0x5E, 0x00, 0x01, 0x01,
                           0x81, 0x00, 0x00, 0x00,                                                                              // vlan tag
                            0x81, 0x00, 0x00, 0x00,                                                                             // vlan tag
                            0x08, 0x00,                                                                                         // type
                            (4 << 4) | 5,                                                                                       // version | length
                            (0x30 << 2) | 0,                                                                                    // dscp & ecn
                            0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0xFF,
                            0x70,                                                                                               // protocol
                            0x19, 0xE1, 0xC0, 0xA8, 0x00, 0x0A, 0xE0, 0x00,
                            0x00, 0x12, 0x21, 0x01, 0xC8, 0x01, 0x00, 0x01, 0x56, 0x52, 0xC0, 0xA8, 0x00, 0x01, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

TestCase vlan2TagCase = {
        .isipv4 = true,
        .optionsPresent = false,
        .protocol = 0x70,
        .dscp = 0x30,
        .packetSize = sizeof(vlan2Tag) / sizeof(vlan2Tag[0]),
        .packet = vlan2Tag
};


TestCase *testCases[] = {&emptyCase,
                         &justEthernetHeaderCase,
                         &notEnoughDataResult,
                         &legacyEthernetCase,
                         &invalidTypeCase,
                         &ipv6Case,
                         &notIpv4TypeCase,
                         &validCase,
                         &vlanTagCase,
                         &vlan2TagCase};

int main() {
    size_t numOfCases = sizeof(testCases) / sizeof(testCases[0]);
    for (int i = 0; i < numOfCases; ++i) {
        testCase(testCases[i]);
    }
}

int testCase(TestCase *testCase) {
    Ipv4Info ipv4Info;
    bool isipv4 = ethIpv4Parse(testCase->packet, testCase->packetSize, &ipv4Info);

    if (isipv4 == testCase->isipv4 &&
            ipv4Info.dscp == testCase->dscp &&
            ipv4Info.protocol == testCase->protocol &&
            ipv4Info.optionsPresent == testCase->optionsPresent) {
        puts("Test passed");
        return true;
    } else {
        puts("Test NOT passed");
        return false;
    }
}
