# Expertize test #
Napišite funkcijo (v C-ju), ki bo iz podanega Ethernet paketa ugotovila, ali gre za IPv4 paket in v tem
primeru zbrala nekaj ključnih podatkov.
Funkcija in struktura s ključnimi podatki naj imata naslednjo deklaracijo:

``` c
typedef struct Ipv4Info
{
    uint8_t dscp;               // IPv4 DSCP field value
    uint8_t protocol;           // IPv4 protocol field value
    bool optionsPresent;        // Are IPv4 options present in the packet?
} Ipv4Info;
```
Implementacija funkcije naj ima naslednje lastnosti:
1. Vhodni podatek v funkcijo buffer vsebuje Ethernet paket, bufLen pa označuje njegovo velikost
v bajtih
2. Funkcija naj vrne true, če je paket IPv4 paket in false v vseh drugih primerih
3. Izhodni podatek iz funkcije (spremenljivka info) naj bo napolnjen s podatki, kot nakazujejo
komentarji ob definiciji strukture, a le če funkcija vrne true. V kolikor ne gre za IPv4 paket naj
podatki ostanejo nespremenjeni.
4. Posebno pozornost posvetite temu, da ima vhodni paket lahko poljubno število VLAN označb.
Poleg implementacije funkcije poskrbite za ustrezne teste enote (angl. unit test), ki bodo prikazali in
preverili delovanje funkcije na podlagi različnih vhodnih podatkov.
V primeru nejasnosti pošljite vprašanje na marko.vaupotic@aviatnet.com.
Veliko sreče pri reševanju naloge.