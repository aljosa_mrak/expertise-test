#include "ethernetParser.h"

uint16_t getType(uint16_t ethernetFrameType) {
    return (ethernetFrameType & 0xff00) >> 8
           | (ethernetFrameType & 0xff) << 8;
}

uint8_t getDscp(uint8_t dscp) {
    return (dscp & 0xf0) >> 4
           | (dscp & 0xf) << 4;
}

bool ethIpv4Parse (const void* buffer, size_t bufLen, Ipv4Info* info) {
    if (bufLen < MIN_ETHERNET_PACKET_LENGTH) {
        puts("Not a valid ethernet packet. Too short");
        return false;
    }

    EthernetHeader *ethernetHeader = (EthernetHeader *) buffer;

    uint16_t type = getType(ethernetHeader->type);

    if (type <= MAX_ETHERNET_LENGTH) {           // IEEE 802.3 frame
        puts("Unsupported Ethernet protocol. IEEE 802.3 frame not supported.");
        return false;
    }
    
    if (type < ETHERNET_V2_TYPE) {
        printf("Invalid type \"%x\"\n", type);
        return false;
    }

    uint16_t dataStartIdx = ETHERNET_DATA_BASE_OFFSET;

    // assume multiple VLAN tags works by stacking them
    while (type == ETHERNET_802_1Q_TYPE || type == ETHERNET_802_1ad_TYPE) {
        type = getType(*(uint16_t*) (buffer + dataStartIdx + ETHERNET_VLAN_TYPE_SHIFT));
        dataStartIdx += ETHERNET_VLAN_TAG_LENGTH;
    }

    if (type != ETHERNET_IP_V4_TYPE) {
        printf("Not an IP v4 type - \"%x\"\n", type);
        return false;
    }
        
    IpHeader *ipHeader = (IpHeader *) (buffer + dataStartIdx);
    printIpHeader(ipHeader);
    if (ipHeader->version == IP_V4) {
        info->protocol = ipHeader->protocol;
        info->dscp = getDscp(ipHeader->dscp);
        info->optionsPresent = ipHeader->headerLength > IP_BASE_HEADER_LENGTH;

        printIpv4Info(info);

        return true;
    }

    printf("Not an IP v4. Version: \"%d\"\n", ipHeader->version);
    return false;
}

void printIpHeader(IpHeader *ipHeader) {
    printf("IP header\n");
    printf("\tVersion:\t\tv%d\n", ipHeader->version);
    printf("\tDSCP:\t\t\t%x\n", getDscp(ipHeader->dscp));
    printf("\tPprotocol:\t\t%x\n", ipHeader->protocol);
    printf("\tHeader length:\t%x\n", ipHeader->headerLength);
}

void printIpv4Info(Ipv4Info *info) {
    printf("IPv4 info\n");
    printf("\tDSCP:\t\t%x\n", info->dscp);
    printf("\tprotocol:\t%x\n", info->protocol);
    if (info->optionsPresent) {
        puts("\tOptions present");
    } else {
        puts("\tOptions not present");
    }
}