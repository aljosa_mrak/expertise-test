#ifndef ETHERNET_PARSER_H
#define ETHERNET_PARSER_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

static const int MAX_ETHERNET_LENGTH = 1500;

static const int ETHERNET_V2_TYPE = 0x0600;

static const int MIN_ETHERNET_PACKET_LENGTH = 64;

static const int ETHERNET_IP_V4_TYPE = 0x0800;

static const int ETHERNET_802_1Q_TYPE = 0x8100;

static const int ETHERNET_802_1ad_TYPE = 0x88a8;

static const int ETHERNET_DATA_BASE_OFFSET = 14;

static const int ETHERNET_VLAN_TAG_LENGTH = 4;

static const int IP_V4 = 4;

static const int IP_BASE_HEADER_LENGTH = 5;

static const int ETHERNET_VLAN_TYPE_SHIFT = 2;
typedef struct Ipv4Info
{
    uint8_t dscp;               // IPv4 DSCP field value
    uint8_t protocol;           // IPv4 protocol field value
    bool optionsPresent;        // Are IPv4 options present in the packet?
}
        Ipv4Info;


typedef struct EthernetHeader
{
    uint8_t source[6];               // MAC source
    uint8_t destination[6];          // MAC destination
    uint16_t type;
}
        EthernetHeader;

typedef struct IpHeader
{
    uint8_t headerLength: 4,
            version: 4;
    uint8_t : 6,
            dscp: 2;
    uint8_t _[7];
    uint8_t protocol;
}
        IpHeader;


bool ethIpv4Parse (const void* buffer, size_t bufLen, Ipv4Info* info);

/*
 * convent to little-endian
 */
uint16_t getType(uint16_t ethernetFrameType);

/*
 * convent to little-endian
 */
uint8_t getDscp(uint8_t dscp);

void printIpHeader(IpHeader *ipHeader);

void printIpv4Info(Ipv4Info *pInfo);

#endif