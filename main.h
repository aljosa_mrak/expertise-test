#ifndef MAIN_H
#define MAIN_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

typedef struct TestCase
{
    bool isipv4;
    uint8_t dscp;               // IPv4 DSCP field value
    uint8_t protocol;           // IPv4 protocol field value
    bool optionsPresent;        // Are IPv4 options present in the packet?
    size_t packetSize;
    unsigned char *packet;
}
        TestCase;

int testCase(TestCase *testCase);

#endif